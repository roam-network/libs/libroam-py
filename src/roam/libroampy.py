
import ctypes
import os
import platform
import sys

libroam = None


#ctypes cheat sheet
#
# ctypes type  | C type                                 | Python type
#-----------------------------------------------------------------------------------
# c_bool       | _Bool                                  | bool (1)
#
# c_char       | char                                   | 1-character bytes object
#
# c_wchar      | wchar_t                                | 1-character string
#
# c_byte       | char                                   | int
#
# c_ubyte      | unsigned char                          | int
#
# c_short      | short                                  | int
#
# c_ushort     | unsigned short                         | int
#
# c_int        | int                                    | int
#
# c_uint       | unsigned int                           | int
#
# c_long       | long                                   | int
#
# c_ulong      | unsigned long                          | int
#
# c_longlong   |  __int64 or long long                  | int
#
# c_ulonglong  | unsigned __int64 or unsigned long long | int
#
# c_size_t     | size_t                                 | int
#
# c_ssize_t    | ssize_t or Py_ssize_t                  | int
#
# c_time_t     | time_t                                 | int
#
# c_float      | float                                  | float
#
# c_double     | double                                 | float
#
# c_longdouble | long double                            | float
#
# c_char_p     | char* (NUL terminated)                 | bytes object or None
#
# c_wchar_p    | wchar_t* (NUL terminated)              | string or None
#
# c_void_p     | void*                                  | int or None





def load_libroam_or_exit(base_path_in = None):

    base_path = os.path.dirname(__file__)
    if base_path_in != None:
        base_path = base_path_in

    # handle libnexa on multiple platforms
    p = platform.platform()
    # TODO update this to handle windows and apple silicon correctly
    if "Linux" in p and "x86_64" in p:
        library_paths = ('libroam.so')  # fall back to system lib, if any
    elif "Windows" in p:
        library_paths = ('libroam.dll')  # on Windows it's in the pyinstaller top level folder, which is in the path
    elif "macOS"in p and "arm64" in p:
        library_paths = ('libroam.dylib')  # on Mac it's in the pyinstaller top level folder, which is in libpath
    elif "macOS"in p and "x86_64" in p:
        library_paths = ('libroam_x86.dylib')
    elif 'ANDROID_DATA' in os.environ:
        # We don't actually use coincurve's Python API, it's just a convenient way to load
        # libroam
        import coincurve  # noqa: F401
        library_paths = 'libroam.so',
    elif sys.platform == 'ios':
        # On iOS, we link secp256k1 directly into the produced binary. We load
        # the current executable as a shared library (this works on darwin/iOS).
        # iOS build note: In Xcode you need to set "Symbols Hidden by Default"
        # to "No" for Debug & Release builds for this to work.
        library_paths =  (sys.executable,)
    libroam = None
    for lp in library_paths:
        try:
            libroam = ctypes.cdll.LoadLibrary(lp)
            if libroam is None:
                raise Error("Cannot find %s shared library", lp)
            break
        except OSError as e:
            print_error("failed to load libroam: Platform  : " + p + " Path: " + lp)
            continue
    if not libroam:
        print_stderr('[libroam] failure: libroam library failed to load')
        sys.exit(0)
    return libroam

def load_libroam(base_path_in = None):
    global libroam
    try:
        if not libroam:
            libroam = load_libroam_or_exit(base_path_in)
    except:
        libroam = None





def roam_get_mainnet_port() -> int:
    port = libroam.roam_get_mainnet_port()
    return port

def roam_get_testnet_port() -> int:
    port = libroam.roam_get_testnet_port()
    return port

# roam uses a sqlite database to store information across application restarts
# strDirName is the directory in which the packet manager database is located
# strFileName is the name of the file in that directory
def roam_initialise(str_dir_name: str, str_file_name: str):
    assert isinstance(str_dir_name, str)
    assert isinstance(str_file_name, str)
    bytes_dir_name = str_dir_name.encode('ascii')
    bytes_file_name = str_file_name.encode('ascii')
    libroam.roam_initalise(bytes_dir_name, len(bytes_dir_name), bytes_file_name, len(bytes_file_name))

def roam_shutdown():
    libroam.roam_shutdown()

# the data is expected to be raw binary data from the network
def roam_process_packet(byte_data: bytes) -> bool:
    res = libroam.roam_process_packet(byte_data, len(byte_data))
    return res

# TODO, might remove this API
'''
// A backup for roam_process_packet that passes in a packet by fields
ROAM_API bool roam_process_packet_from_data(uint8_t packet_version,
    uint16_t dest_buffer_id,
    uint16_t source_buffer_id,
    uint64_t packet_total_length,
    uint8_t* packet_checksum,
    uint64_t packet_checksum_len,
    uint64_t salt,
    uint8_t* sender_pubkey,
    uint64_t sender_pubkey_len,
    uint8_t* sender_signature,
    uint64_t sender_signature_len,
    uint8_t* packet_data,
    uint64_t packet_data_len);
'''

# bool_reject_unsigned rejects messages that are not signed if enabled
def roam_register_new_application(bool_reject_unsigned: bool) -> bytes, int:
    str_pubkey = create_string_buffer(1000)
    registered_port = libroam.roam_register_new_application(bool_reject_unsigned, str_pubkey, 1000)
    # TODO - change the roam_register_new_application API in the c++ side, this implementation is terrible
    end_byte = 0
    for byte in str_pubkey.raw:
        if byte == "\x00":
            break
        end_byte = end_byte + 1
    return str_pubkey.raw[0:end_byte], registered_port


def roam_release_registered_application(int_port: int) -> bool:
    res = libroam.roam_release_registered_application(int_port)
    return res


def roam_add_tag(byte_data: bytes) -> bool:
    res = libroam.roam_add_tag(byte_data, len(byte_data))
    return res


# TODO - might remove this API
'''
ROAM_API bool roam_add_tag_from_data(bool bool_is_private,
    const uint8_t* pubkey_data,
    const uint64_t pubkey_data_len,
    const uint8_t* privkey_data,
    const uint64_t privkey_data_len);
'''

# byte_data is always len 20 because it is a hash160 (sha256 + ripemd160) of the tag
# not the full tag itself
def roam_have_tag(byte_data: bytes) -> bool:
    assert(len(byte_data) == 20)
    # TODO - verify if i need to copy the bytes into a create_string_buffer created buffer
    res = libroam.roam_have_tag(byte_data)
    return res

# Note - max_data_len needs to be divisible by 8
def roam_get_buffer_ids_with_new_data() -> list[int]:
    res_buffer = create_string_buffer(10000)
    num_ids = libroam.roam_get_buffer_ids_with_new_data(res_buffer, 10000)
    list_ids = []
    for i in range(num_ids):
        buffer_id = int.from_bytes(res_buffer.raw[ i*8 : (i+1)*8 ], sys.byteorder, signed=False)
        list_ids.append(buffer_id)
    return list_ids


def roam_create_message(bytes_pubkey: bytes, int_protocol_id: int, bytes_data: bytes) -> bytes:
    res_buffer = create_string_buffer(1000)
    int_msg_byte_len = libroam.roma_create_message(bytes_pubkey, len(bytes_pubkey), int_protocol_id, bytes_data, len(bytes_data), res_buffer, 1000)
    return res_buffer[0:int_msg_byte_len]

'''
ROAM_API int64_t roam_create_message_sig(const uint8_t* pubkey, uint64_t len_pubkey, const uint16_t &n_protocol_id,
    const uint8_t* data, uint64_t len_data, const char* c_str_sender_pubkey, uint64_t len_sender_pubkey,
    const uint8_t* signature, uint64_t len_signature, uint8_t* ret, const uint64_t max_len_ret);
def roam_create_message_sig() -> bytes:
    pass
'''

def roam_read_messages(int_protocol_id: int) -> bytes:
    res_buffer = create_string_buffer(10000)
    int_msg_byte_len = libroam.roam_read_messages(int_protocol_id, res_buffer, 10000)
    return res_buffer.raw[0:int_msg_byte_len]

def roam_record_request_origin(int_nonce: int, bytes_source_pubkey: bytes):
    libroam.roam_record_request_origin(int_noncce, bytes_source_pubkey, len(bytes_source_pubkey))


def roam_get_request_origin(int_nonce: int) -> bytes:
    res_buffer = create_string_buffer(1000)
    int_pubkey_byte_len = libroam.roam_get_request_origin(int_nonce, res_buffer, 1000)
    return res_buffer.raw[0:int_pubkey_byte_len]


def roam_record_route_to_peer(bytes_search_pubkey: bytes, int_node: int):
    libroam.roam_record_route_to_peer(bytes_search_pubkey, len(bytes_search_pubkey, int_node))


def roam_have_route_to_peer(bytes_search_pubkey: bytes) -> bool:
    res = libroam.roam_have_route_to_peer(bytes_search_pubkey, len(bytes_search_pubkey))
    return res


def roam_get_route_to_peer(bytes_search_pubkey: bytes) -> bool,int:
    node_id = 0
    res = libroam.roam_get_route_to_peer(bytes_search_pubkey, len(bytes_search_pubkey), node_id)
    if res:
        return True, node_id
    return False, 0


def roam_get_public_tag_hex():
    res_buffer = create_string_buffer(1000)
    int_hex_byte_len = libroam.roam_get_public_tag_hex(res_buffer, 1000)
    return res_buffer.raw[0:int_hex_byte_len]


def roam_sign_with_public_tag(bytes_message: bytes):
    res_buffer = create_string_buffer(1000)
    int_sig_byte_len = libroam.roam_sign_with_public_tag(bytes_message, len(bytes_message), res_buffer, 1000)
    return res_buffer.raw[0:int_sig_byte_len]


def roam_is_old_request(int_nonce: int) -> bool:
    res = libroam.roam_is_old_request(int_nonce)
    return res


def roam_add_new_request_time_data(int_nonce: int):
    libroam.roam_add_new_request_time_data(int_nonce)
